# Project description

Flashcard app that lets you practice plural forms of German nouns. You can choose to practice by just flipping the flashcards or typing out your guess to see if you got it correct. 
Flashcards are shown in random order. You can apply certain filters to only practice certain batch of words.
There is also search field where you can type a specific word you are looking for and you can immediately see the plural form by clicking on 'Search'.
There are two implemented shortcuts.

[![Netlify Status](https://api.netlify.com/api/v1/badges/9330dcfe-a471-4cc9-8261-245bb459e018/deploy-status)](https://app.netlify.com/sites/competent-allen-9f2aa2/deploys)

Plural forms of nouns were obtained by implementing web automation using **puppeteer** library by Google. Future improvement of the project envisages hard-coding the API to add more filtering options (words by levels, categories; practicing articles etc.)

### **Javascript:**

 1. Used ES6 features:
    - Arrow functions,
    - Destructuring,
    - spread operator,
    - let etc.
    

 2. Used string methods: 
    - `slice()`
    - `charAt()`
    - `startsWith()`
    - `endsWith()`
    - `toUpperCase()`
    - `toLowerCase()`
    - `parseInt()`
    - `trim()`
    
 3. Used array methods:
    - `push()`
    - `some()`
    - `filter()`
    - `indexOf()`
    - `forEach()`
    - `concat()`

    
 ### **React:**
 
 The application is written using functional components and hooks.
 To see the code for all components click [here](https://gitlab.com/rasulova1995/germanplural/-/tree/master/src/components).
 
 1. Used hooks:
    - `createContext()`
    - `useState()`
    - `useEffect()`
    - `useContext()`
    - `useRef()`

Original list of nouns in the form of dictionary can be found [here](https://github.com/hathibelagal/German-English-JSON-Dictionary/blob/master/german_english.json).

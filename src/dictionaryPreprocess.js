// Pre-process words
import dictionary from "./dictionary";
import plurals from "./plurals";

let translations = [];
let singulars = [];
for (let word of dictionary) {
    translations = translations.concat(Object.values(word))
}
for (let word of dictionary) {
    singulars = singulars.concat(Object.keys(word))
}
let fullProcessedDictionary = [];
for (let i = 0; i < dictionary.length; i++) {
    let exclude = ['none', '-n', '-', 'am'];
    if (!exclude.some((letters) => plurals[i].startsWith(letters))) {
        fullProcessedDictionary.push({
            front: singulars[i],
            back: plurals[i],
            translation: translations[i]
        })
    }
}

export default fullProcessedDictionary
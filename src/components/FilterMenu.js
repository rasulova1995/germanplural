import React, {useContext, useEffect, useRef, useState} from 'react';
import WordContext from "./WordContext";
import fullProcessedDictionary from "../dictionaryPreprocess";

const FilterMenu = () => {
    const [start, setStart] = useState('');
    const [end, setEnd] = useState('');
    const [length, setLength] = useState('');

    const wordContext = useRef(useContext(WordContext));

    const startHandler = (event) => {
        setStart(event.target.value)
    };

    const endHandler = (event) => {
        setEnd(event.target.value)
    };

    const lengthHandler = (event) => {
        setLength(event.target.value)
    };

    // Filter words

    useEffect(() => {

        let isStart = (anyWord) => {
            let firstLetterCapitalized = start.charAt(0).toUpperCase();
            let transformedWord = firstLetterCapitalized + start.slice(1);
            return anyWord.front.startsWith(transformedWord)
        };
        let isEnd = (anyWord) => (anyWord.front.endsWith(end.toLowerCase()));
        let isLength = (anyWord) => (anyWord.front.length === parseInt(length));

        let isStartEndLength = (anyWord) => (isStart(anyWord) && isEnd(anyWord) && isLength(anyWord));

        let isStartEnd = (anyWord) => (isStart(anyWord) && isEnd(anyWord));
        let isStartLength = (anyWord) => (isStart(anyWord) && isLength(anyWord));
        let isEndLength = (anyWord) => (isEnd(anyWord) && isLength(anyWord));

        // Minimizing this part using callback function

        const filterFunc = (cb) => {
            if (fullProcessedDictionary.some(word => cb(word))) {
                let wordsToShow = fullProcessedDictionary.filter(word => cb(word));
                let indexesToShow = [];
                wordsToShow.forEach(word => indexesToShow.push(fullProcessedDictionary.indexOf(word)));
                wordContext.current.updateState({
                    cardsIndexes: indexesToShow,
                    currentCardIndex: indexesToShow[0],
                })
            } else {
                alert('No such word. Use another filter!')
            }
        };

        if (start && end && length) {
            filterFunc(isStartEndLength)
        } else if (start && end) {
            filterFunc(isStartEnd)
        } else if (start && length) {
            filterFunc(isStartLength)
        } else if (end && length) {
            filterFunc(isEndLength)
        } else if (start) {
            filterFunc(isStart)
        } else if (end) {
            filterFunc(isEnd)
        } else if (length) {
            filterFunc(isLength)
        } else {
            wordContext.current.updateState({
                cards: fullProcessedDictionary
            })
        }

    }, [start, end, length]);

    return (
        <div className='filter-menu'>
            <h2 className='filter-menu-header'>Filter words</h2>
            <h3 className='filter-name'>that start with</h3>
            <input onChange={startHandler}
                   className='filter-input'
                   name='filter'
                   type='text'
                   placeholder='any letter'/>
            <h3 className='filter-name'>that end with</h3>
            <input onChange={endHandler}
                   className='filter-input'
                   name='filter'
                   type='text'
                   placeholder='any letter'/>
            <h3 className='filter-name'>with word length</h3>
            <input onChange={lengthHandler}
                   className='filter-input'
                   name='filter'
                   type='text'
                   placeholder='any length'/>
        </div>
    );
};

export default FilterMenu;
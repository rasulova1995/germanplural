import React, {useState} from 'react';
import fullProcessedDictionary from "../dictionaryPreprocess";

const SearchWord = () => {
    const [enteredSingular, setSingular] = useState('');
    const [foundPlural, setPlural] = useState('');


    const changeHandler = (event) => {
        setSingular(event.target.value);
        setPlural('')
    };

    const searchHandler = () => {
        let wordToSearch = enteredSingular.trim();
        let firstLetterCapitalized = wordToSearch.charAt(0).toUpperCase();
        wordToSearch = firstLetterCapitalized + wordToSearch.slice(1);
        const isWordFound = fullProcessedDictionary.some(word => word.front === wordToSearch);

        if (isWordFound) {
            const [foundWord] = fullProcessedDictionary.filter(word => word.front === wordToSearch);
            setPlural(foundWord.back);
        } else {
            alert('There is no such word. Search for another one!')
        }
    };

    return (
        <div className='search-section'>
            <h4 className='search-section-header'> Search word </h4>
            <input type='text'
                   placeholder='Enter singular form here'
                   onChange={changeHandler}
                   className='search-input'
                   value={enteredSingular}/>
            <button onClick={searchHandler}
                    className={'search-btn'}> Search</button>
            <div className='search-line'>
                <label className='search-line-head'> Singular form : </label>
                <span className='singular'> {enteredSingular} </span>
            </div>
            <div className='search-line'>
                <label className='search-line-head'> Plural form : </label>
                <span className='plural'> {foundPlural} </span>
            </div>
        </div>
    );
};

export default SearchWord;
import React, {createContext, useState} from 'react';
import fullProcessedDictionary from "../dictionaryPreprocess";
import Keyboard from "./Keyboard";

const WordContext = createContext();
const indexesToShow = [];
fullProcessedDictionary.forEach(word => indexesToShow.push(fullProcessedDictionary.indexOf(word)));

export const WordContextProvider = (props) => {
    const [state, setState] = useState({
        frontShowing: true,
        backShowing: false,
        currentCardIndex: 0,
        cardsIndexes: indexesToShow,
        cards: fullProcessedDictionary
    });

    let updateState = (newState) => {
        setState({...state, ...newState})
    };

    const generateIndex = () => {
        let randomNum = Math.floor(Math.random() * state.cardsIndexes.length);
        return state.cardsIndexes[randomNum]
    };

    const showOneFront = () => state.cards[state.currentCardIndex].front;
    const showOneBack = () => state.cards[state.currentCardIndex].back;
    const showTranslation = () => state.cards[state.currentCardIndex].translation;

    const showNewCard = () => {
        setState(prevState => {
            return {
                ...prevState,
                currentCardIndex: generateIndex(),
                frontShowing: true,
                backShowing: false
            }
        });
    };

    const viewChange = (event) => {
        setState(prevState => {
            return {
                ...prevState,
                frontShowing: false,
                backShowing: true
            }
        });
    };

    const showCard = () => {
        let toShow;
        let translation;
        if (state.frontShowing) {
            toShow = showOneFront();
            translation = showTranslation()
        } else {
            toShow = showOneBack()
        }
        return <div className='word-area'>
            <span className='to-show'> {toShow} </span> <br/>
            <span className='translation'> {translation} </span>
            <Keyboard/>
        </div>
    };

    // Everything that goes inside the value is WordContext object.
    return (
        <WordContext.Provider value={{
            state,
            updateState,
            generateIndex,
            showOneFront,
            showOneBack,
            showTranslation,
            showNewCard,
            viewChange,
            showCard
        }}>
            {props.children}
        </WordContext.Provider>
    );
};

export default WordContext;
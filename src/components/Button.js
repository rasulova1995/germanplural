import React, {useContext, useEffect, useRef} from 'react';
import WordContext from "./WordContext";

const Button = () => {
    const nextButton = useRef();
    const wordContext = useContext(WordContext);

    useEffect(() => {
        window.addEventListener('keydown', event => {
            if (event.key === 'Enter' && !event.target.classList.contains('plural-input') && !event.target.classList.contains('check-btn')) {
                nextButton.current.click();
            }
        })
    }, []);

    return (
                <button ref={nextButton}
                        onClick={wordContext.showNewCard}
                        className={wordContext.state.frontShowing ? 'button button-front'
                            : 'button button-back'}> Next
                </button>
        );
};

export default Button;
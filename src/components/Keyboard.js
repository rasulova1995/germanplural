import React, {useState} from 'react';
import keyboardImg from "../images/keyboard.png";

const Keyboard = () => {
    const[keyboardInfoOpen, toggleKeyboardInfo] = useState(false);
    const [keyboardImgHovered, toggleHover] = useState(false);

    const clickHandler = (event) => {
        if (event.target.classList.contains('keyboard-img')) {
            event.stopPropagation();
            toggleKeyboardInfo(!keyboardInfoOpen);
        }
    };

    const hoverHandler = (event) => {
        if (event.target.classList.contains('keyboard-img')) {
            toggleHover(true);
        }
    };

    const noHoverHandler = (event) => {
        if (event.target.classList.contains('keyboard-img')) {
            toggleHover(false);
        }
    };

    return (
        <div className='keyboard'>
            <img onClick={clickHandler}
                 alt='keyboard'
                 onMouseOver={hoverHandler}
                 onMouseOut={noHoverHandler}
                 className='keyboard-img'
                 src={keyboardImg}
                 width={30}
                 height={30}/>

            {keyboardInfoOpen ?
                <div className='keyboard-info'>
                    <div className='keyboard-point'>
                        <span className='action'> Next word </span>
                        <span className='key'> Enter </span></div>
                    <div className='keyboard-point'>
                        <span className='action'> Flip </span>
                        <span className='key'> R </span></div>
                </div> : null}

            {keyboardImgHovered ?
                <div className='keyboard-hover'> Keyboard Shortcuts </div> : null
            }
        </div>
    );
};

export default Keyboard;
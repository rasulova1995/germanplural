import React, {useContext, useEffect, useState, useRef} from "react";
import ModeContext from "./ModeContext";
import WordContext from "./WordContext";
import Flashcard from "./Flashcard";
import Button from "./Button";

const FlashcardViewer = () => {
    const modeContext = useContext(ModeContext);
    const wordContext = useContext(WordContext);
    const [input, setInput] = useState('');
    const [guessed, setGuessed] = useState('guessing');
    const checkButton = useRef();


    const modeHandler = (event) => {
        if (event.target.value === 'flipper') {
            modeContext.setAsFlipper()
        } else if (event.target.value === 'typer') {
            modeContext.setAsTyper()
        }
    };

    const inputChanger = (event) => {
        let enteredPlural = event.target.value.trim();
        let capitalized = enteredPlural.charAt(0).toUpperCase();
        enteredPlural = capitalized + enteredPlural.slice(1);
        setInput(enteredPlural);
        setGuessed('guessing');
    };

    const answerCheck = () => {
        let realPlural = wordContext.showOneBack();
        wordContext.viewChange();
        if (input === realPlural) {
            setGuessed('guessed')
        } else {
            setGuessed('failed')
        }
    };

    useEffect(() => {
        setGuessed('guessing');
        setInput('')
    }, [wordContext.state.currentCardIndex]);

    return (
        <div className='flashcard-viewer'>
            <label htmlFor='modes'
                   className='choose-mode'> Choose your mode </label>
            <select id='modes'
                    onChange={modeHandler}>
                <option value="flipper"> Flip flashcards</option>
                <option value="typer"> Type answers</option>
            </select>

            <h1 className='page-header'> {modeContext.isFlipper ? 'Flashcard Flipper'
                : 'Flashcard Guesser'} </h1>

            <Flashcard guessed={guessed}/>
            {!modeContext.isFlipper ?
                <>
                <input placeholder='Type the plural form here'
                       onChange={inputChanger}
                       className='plural-input'
                       type='text'
                       value={input}/>
                <button className={'check-btn'}
                        ref={checkButton}
                        onClick={answerCheck}>Check
                </button>
            </> : null}
            <Button/>
        </div>
    )
};

export default FlashcardViewer

import React, {createContext, useState} from 'react';

const ModeContext = createContext();

export const ModeContextProvider = (props) => {
    const [isFlipper, setMode] = useState(true);

    const setAsTyper = () => {
        setMode(false)
    };

    const setAsFlipper = () => {
        setMode(true)
    };

    return (
        <ModeContext.Provider value={{isFlipper, setAsFlipper, setAsTyper}}>
            {props.children}
        </ModeContext.Provider>
    );
};

export default ModeContext;
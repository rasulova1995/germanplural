import React from 'react';

const Footer = () => {
    return (
        <div className='page-footer'>
            <div>
                Icon made by
                <a target={"_blank"}
                   href="https://www.flaticon.com/authors/freepik"
                   className={'footer-link'}> Freepik </a> from
                <a target={"_blank"} href={"https://www.flaticon.com/"}
                   className='footer-link'> www.flaticon.com </a></div>

            <div className='developed-by'>
                Developed by <span className={'developed-by name'}>Firuza Rasulova</span></div>

            <div>
                &#60;/&#62; Click
                <a target={"_blank"} 
                href={"https://gitlab.com/rasulova1995/germanplural"}
                   className={' source-code-link'}> here </a> to view the source code on Gitlab</div>
        </div>
    );
};

export default Footer;
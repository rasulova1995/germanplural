import React from 'react';
import germanFlag from '../germany.png'

const Header = () => {
    return (
            <div className='header-container'>
                <div className='description'> A flashcard tool to help you master plural forms of German nouns. </div>
                <div className='logo-part'>
                    <img className='german-flag' src={germanFlag} alt='german-flag'/>
                    <span className='logo-text'>DE-PLURALS</span>
                </div>
            </div>
    );
};

export default Header;
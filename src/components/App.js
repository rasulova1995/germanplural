import React from 'react';
import '../App.css';
import {WordContextProvider} from "./WordContext";
import {ModeContextProvider} from "./ModeContext";
import Header from "./Header";
import Footer from "./Footer";
import FilterMenu from "./FilterMenu";
import FlashcardViewer from "./FlashcardViewer";
import SearchWord from "./SearchWord";

const App = () => {

    return (
        <ModeContextProvider>
            <WordContextProvider>
                <div className='page-container'>
                    <Header/>
                    <div className='main-container'>
                        <div className='sidebar'>
                            <FilterMenu/>
                            <SearchWord/>
                        </div>
                        <FlashcardViewer/>
                    </div>
                    <Footer/>
                </div>
            </WordContextProvider>
        </ModeContextProvider>

    )
};

export default App;

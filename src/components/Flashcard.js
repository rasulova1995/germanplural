import React, {useEffect, useContext, useRef} from 'react';
import WordContext from "./WordContext";

const Flashcard = (props) => {

    const flashCard = useRef();
    const wordContext = useContext(WordContext);

    useEffect(() => {
        window.addEventListener('keydown', event => {
            if (((event.key === 'r' || event.key === 'R') && !event.target.classList.contains('plural-input') && !event.target.classList.contains('search-input')) ||
                (event.type === 'click' && event.target.classList.contains('card'))) {
                flashCard.current.click();
            }
        })
    }, []);

    return (
        <>
            <div ref={flashCard} className={wordContext.state.frontShowing ? 'card front-card'
                : props.guessed === 'guessed' ? 'card back-card input-guessed'
                    : props.guessed === 'failed' ? 'card back-card input-failed'
                        : 'card back-card'}
                 onClick={wordContext.viewChange}>{wordContext.showCard()}</div>
        </>
    );
};

export default Flashcard;